<div class='white'></div>

<header>
	<img src="<?= FRONT_ASSETS ?>img/gaps.png">
</header>

<div class='clear'></div>

<script type="text/javascript">
	
	openAnim();

  function openAnim() {
      $('.white').fadeOut(500);

      setTimeout(function(){
          $('.welcome').fadeIn(1000);
          $('.welcome').css('margin-top', '700px');
      }, 500 );

      setTimeout(function(){
        $('.menu, .message').fadeIn(1000);
        $('.menu').css('margin-bottom', '0');
        $('.welcome').css('margin-top', '370px');
      }, 2000);
  }
</script>