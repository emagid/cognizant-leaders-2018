<?php

namespace Model;

use Emagid\Core\Model;

class Register_Request extends Model{
    static $tablename = 'register_request';
    static $fields = [
        'first_name',
        'last_name',
        'company',
        'phone',
        'email',
        'full_address',
        'status' //0->pending, 1->approved, 2->rejected
    ];
}