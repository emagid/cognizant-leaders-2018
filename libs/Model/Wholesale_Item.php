<?php

namespace Model;

use Emagid\Core\Model;

class Wholesale_Item extends Model{
	static $tablename = 'wholesale_items';
	static $fields = [
		"wholesale_id",
		"product_id",
		"price"
	];
}