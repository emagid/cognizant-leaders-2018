<?php

namespace Model;

class Map extends \Emagid\Core\Model {
    static $tablename = "public.map";

    public static $fields  =  [
        'map_image',
        'display_order' => ['type'=>'numeric'],
    ];

}