<?php

class Engagement_Ring extends \Emagid\Core\Model{

    static $fields = [
        'name',
        'default_metal',
        'default_color',
        'dwt',
        'semi_mount_weight',
        'stone_breakdown',
        'center_stone_weight',
        'center_stone_shape',
        'total_stones',
        'semi_mount_diamond_weight',
        'diamond_color',
        'diamond_quality',
        'cost_per_dwt',
        'fourteenkt_gold_price',
        'eighteenkt_gold_price',
        'platinum_price',
        'website_fourteenkt_gold_price',
        'website_eighteenkt_gold_price',
        'website_platinum_price',
        'slug'
    ];

    public function getPrice($selectMetal)
    {
        switch(strtoupper($selectMetal)){
            case '14KT GOLD':
                return $this->price;
            case '18KT GOLD':
                return $this->formatPrice($this->website_eighteenkt_gold_price);
                break;
            case 'PLATINUM':
                return $this->formatPrice($this->website_platinum_price);
        }
    }

    private function formatPrice($string)
    {
        $string = str_replace(['$', ','], '', $string);
        $string = trim($string);
        return floatval($string);
    }
}