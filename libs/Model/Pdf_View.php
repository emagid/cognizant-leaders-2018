<?php

namespace Model;

class Pdf_View extends \Emagid\Core\Model {
    static $tablename = "public.pdf_view_counts";

    public static $fields  =  [
        'phone',
        'pdf_no',
        'view_type',
    ];
}