<?php 

namespace Model; 

class Page extends \Emagid\Core\Model {

	static $tablename = 'page'; 
	
	public static $fields =  [
		'title', 
		'slug',
		'description', 
		'featured_image',
		'meta_title', 
		'meta_keywords', 
		'meta_description',
		'date_modified',
		'content'
	];
	
}