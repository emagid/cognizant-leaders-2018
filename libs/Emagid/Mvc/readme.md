Controller
the controller will be the link between models and views

creating a controller

class <controller_name>Controller extends \Emagid\Mvc\Controller {}

all controllers must end with 'Controller' as their name.

they all belong in the controllers folder, located in your project folder

if you would like group controllers into a specific namespace, then you can create a folder with the name the same as the namespace name for those controllers.

Example:
	to create the namespace Admin, first create under the controllers folder another folder called Admin, then you can put all your controllers here that will have the namespace Admin


in each __construct() function for a controller, you can specify to use a different template by setting the template name like this:
	$this->template = <template_name>
	the template will be located under controllers/<template_name>/<template_name>.php

in each function of a controller that corresponds to a view, you can load the view file
 by calling $this->loadView().
 you can pass parameters to the loadView function.

 one parameter is optional and is the name of the view file to obtain. if this is not specified then it will attempt to load the view file with the same name as the current controller function
 the second parameter is data that can be passed to the view file. the type of variable this data can be sent as can be anything, but it is recommende to use teh stdClass(), as it has a better way to access variables.

 if you only wish to pass data, you can call the function like this: loadView($data).


 View
 views are stored in the views folder, which is located on the project folder.
 since each view is associated to a controller function, you will need to create inside the views folder different folders for each controller you have, with the name for each folder being the name of the controller, without the Controller part.

 example:
 	if you have usersController, then under views folder create another folder named users, and in this folder you will store all your views associated to the usersController functions

if you have namespaced controllers, then first create a folder under the views folder and name it the same as your namespace name, then you can put all folders inside this folder for each controller with that namespace.

example:
	for usersController with namespace Admin, we will have the following folder structure:
	->project folder
		->views
			->Admin
				->users



Routes:
routes are stored in an array like this:
array(
'routes'=>[
 [
      'name' => <route_name>,
      'pattern' =><url_pattern>,
      'controller' => <controller_name>,
      'action' => <function to execute>
    ],
    // another route
    [
      'name' => <route_name>,
      'area'=><namespace name>
      'pattern' =><url_pattern>,
      'controller' => <controller_name>,
      'action' => <function to execute>
    ],
]
)

the MVC will attempt to locate the url pattern inside one of the route arrays from the 'pattern' value. it will always get the first route if successful even if other patterns match the url pattern.
when successful it tells the mvc to load the specified controller and execute the specified action function.
an area basically tells the route to look into one of the folders inside the controller folder to look for controllers there.

Types of URL parts : 
controller   - constant value 
{controller} - mandatory variable 
{?controller}- optional variable 
{model:filed}- will pass an instance of the Model using the value from the url as filter.

Examples
optional values
[
	'name'       => 'my_route', 
	'pattern'    => {?controller}/{?action}/{?id}, 
	'controller' => 'home',   // setting up the defualt value for that variable.
	'action'     => 'index'
]

some mandatory and some optional
[
	'name'       => 'my_route', 
	'pattern'    => {controller}/{?action}/{?id}, 
]

using static parts
[
	'name'       => 'my_route', 
	'pattern'    => Books/{?genre}/{?shelf}/{?id}, 
	'controller' => 'books', 
	'action'     => 'list'
]

mixing parts
[
	'name'       => 'my_route', 
	'pattern'    => {genre}/{shelf}/Books/{?id}, 
	'controller' => 'books', 
	'action'     => 'list'
]

using models
[
	'name'       => 'my_route', 
	'pattern'    => Books/{?\Model\Book:genre}/{?shelf}/{?id}, 
	'controller' => 'books', 
	'action'     => 'list'
]

example : 
Books/comedy - 
    will call class 'booksController' , function : 'list' it will pass the array: 

    it will use 'genre=comedy' as filter on the model \Model\Book : \Model\Book:getList(['where'=>['genre'=>'comedy']])

    so what it will pass to the function is 
	['model' => \Model\Book:getList(['where'=>['genre'=>'comedy']])]