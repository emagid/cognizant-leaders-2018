<?php
 if(count($model->article_ratings)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
            <th width="15%">Email</th>
            <th width="15%">Article</th>
            <th width="15%">Rating / Score</th>	
        </tr>
      </thead>
      <tbody>
        <?php foreach($model->article_ratings as $obj){ ?> 
          <tr>
              <td><a href="#"><?php echo $obj->email ?></a></td>
              <td><a href="#">Article <?php echo $obj->article; ?></a></td>
              <td><a href="#"><?php echo $obj->rating; ?></a></td>
          </tr>
        <?php } ?> 
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'pdf_views';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>