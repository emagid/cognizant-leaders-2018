<?php
 if(count($model->pdf_views)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
            <th width="15%">PDF Number</th>
            <th width="15%">Total Downloads</th>	
        </tr>
      </thead>
      <tbody>
        <?php foreach($model->pdf_view_counts as $obj){ ?> 
            <tr>
                <td><?php echo $obj['pdf_no'] ?></a></td>
                <td><?php echo $obj['view_counts']; ?></td>
            </tr>
        <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'pdf_views';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>