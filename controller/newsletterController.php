<?php

class newsletterController extends siteController {
	
	public function add(){
		foreach(\Model\Newsletter::getCategories() as $category) {
			$newsletter = new \Model\Newsletter();
			$newsletter->email = $_POST['email'];
			$newsletter->category = $category;
			$newsletter->is_subscribe = "1";
			$newsletter->save();
		}

		$email = new \Emagid\Email();
		$email->addTo($newsletter->email);
		$email->subject('Newsletter subscription success');
		$email->body = '<p><a href="www.emagidCheckin.com"><img src="http://beta.emagidCheckin.com/content/frontend/img/logo.png" /></a></p>'
			.'<p>Thanks for signing up to receive our newsletter! We\'ll keep you informed on the hottest trends and insider deals at emagidCheckin.com.</p>'
			.'<p>Regards,<br />emagidCheckin Team</p>'
			.'<p>Find us on <a href="https://www.facebook.com/pages/americangrowndiamondscom/202088860134">Facebook</a> and <a href="https://twitter.com/emagidCheckin">Twitter</a>.</p>';
		$email->send();

		echo "Success";
	}

	private function addToMailChimp($email){
		$apikey = '8edca9a3efd78215517b6b9a9d85b385-us11';
        $auth = base64_encode( 'user:'.$apikey );

        $data = array(
            'apikey'        => $apikey,
            'email_address' => $email,
            'status'        => 'subscribed'
        );
        $json_data = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://us11.api.mailchimp.com/3.0/lists/be65f4b3f2/members/');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.$auth));
        curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);                                                                                      

        $result = curl_exec($ch);

        //dd(json_decode($result));

        // var_dump($result);
        // die('Mailchimp executed');
	}

}