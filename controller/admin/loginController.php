<?php

use Emagid\Core\Membership;

/**
 * Base class to controll login
 */

class loginController extends \Emagid\Mvc\Controller {
	
	public function login(){
		if(isset($_SESSION['em_authentication_id'])){
			$logged_admin = \Model\Admin::getItem(\Emagid\Core\Membership::userId());
			if($logged_admin!=null) {
				redirect('/admin/dashboard/index');
			}
		}

		$model = new \stdClass();
		$model->page_title = 'Login';
		$model->logged_admin = null;
		$model->admin_sections = [];
		$this->loadView($model);
	}
	
	public function login_post()
	{	
		if (\Model\Admin::login($_POST['username'],$_POST['password'])){
			redirect(ADMIN_URL . 'dashboard/index');
		} 
		/* 
		$admin = new \Model\Admin();
		$admin->first_name = 'Master';
		$admin->last_name = 'User';
		$admin->email = 'master@user.com';
		$admin->username = 'emagid';
		$admin->password = 'test5343';
		$admin->permissions = 'Administrators';

		$hash = Membership::hash($admin->password);
		$admin->password = $hash['password'];
		$admin->hash = $hash['salt'];

		if ($admin->save()){
			$adminRoles = \Model\Admin_Roles::getList(['where' => 'active = 1 and role_id = 1 and admin_id = '.$admin->id]);
			if (count($adminRoles) <= 0){
				$adminRole = new \Model\Admin_Roles();
				$adminRole->role_id = 1;
				$adminRole->admin_id = $admin->id;
				$adminRole->save();
			}
		};
		echo 'BITCH';*/
		
		$model = new \stdClass();
		$model->errors = ['Invalid username or password'];
		$model->page_title = 'Login';
		$model->logged_admin = null;
		$model->admin_sections = [];
		$this->loadView($model);
	}

	public function logout(){
		Membership::destroyAuthenticationSession();
		redirect(ADMIN_URL . 'login');
	}

}