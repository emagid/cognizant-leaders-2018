<?php

class app_downloadsController extends adminController {
	
	function __construct(){
		parent::__construct("App_Download");
	}
	
	function index(Array $params = []){
		// $this->_viewData->hasCreateBtn = true;
		$this->_viewData->apple_downloads_count = \Model\App_Download::getCount(['where'=>"app_store = 'apple'"]);
		$this->_viewData->google_downloads_count = \Model\App_Download::getCount(['where'=>"app_store = 'google'"]);
		parent::index($params);
	}
}