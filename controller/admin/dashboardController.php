<?php

use Emagid\Core\Membership,
Emagid\Html\Form,
Model\Admin,
Model\Banner; 

class dashboardController extends adminController {
	
	function __construct() {
        parent::__construct('Dashboard');
    }

	public function index(Array $params = []){
        $sql = "SELECT * FROM (".
                " (SELECT gif.id as gif_id, NULL AS pic_id, gif.image AS gif_url, NULL as pic_url, gif.insert_time FROM gif WHERE gif.active = 1)".
                " UNION ALL".
                " (SELECT NULL AS gif_id, Snapshot_Contact.id as pic_id, NULL as gif_url, '/content/uploads/Snapshots/' || Snapshot_Contact.image as pic_url, Snapshot_Contact.insert_time FROM snapshot_contact WHERE snapshot_contact.active = 1)".
                ") results".
                " ORDER BY insert_time DESC LIMIT 5";

        global $emagid;

        $db = $emagid->getDb();


        $latest = $db->execute($sql) ;
        $this->_viewData->latest = $latest;

		$this->_viewData->page_title = 'Dashboard';
		$this->loadView($this->_viewData);
	}

}